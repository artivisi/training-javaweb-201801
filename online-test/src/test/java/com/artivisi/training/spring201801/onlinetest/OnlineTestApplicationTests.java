package com.artivisi.training.spring201801.onlinetest;

import com.artivisi.training.spring201801.onlinetest.dao.PesertaDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OnlineTestApplicationTests {

	@Autowired private PasswordEncoder passwordEncoder;
	@Autowired private PesertaDao pesertaDao;

	@Test
	public void testBCryptPassword() {
		String password = "rahasia";
		String hashed = passwordEncoder.encode(password);
		Assert.assertNotNull(hashed);
		System.out.println("BCrypted : "+hashed);

		// cobacoba : $2a$10$BucJJkaqhRyKT4H8Yhv33u.suipe0dbmSEfWCmr9J9PnzyzZD76SS
		// rahasia : $2a$10$XSLWKxslG628cFas6BbZ6uenvcTs9uxa5fw0ZVgipYKlrboUZHlDm
	}

}
