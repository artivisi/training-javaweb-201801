package com.artivisi.training.spring201801.onlinetest.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HalamanLogin {

    @FindBy(name="username")
    private WebElement username;

    @FindBy(name="password")
    private WebElement password;

    public void isiUsername(String u) {
        username.sendKeys(u);
    }

    public void isiPassword(String p) {
        password.sendKeys(p);
    }

    public void login() {
        password.submit();
    }

    public HalamanLogin(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }
}
