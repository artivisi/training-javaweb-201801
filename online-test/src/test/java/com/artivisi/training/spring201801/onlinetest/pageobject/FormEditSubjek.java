package com.artivisi.training.spring201801.onlinetest.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FormEditSubjek {

    @FindBy(name="kode")
    private WebElement kode;

    @FindBy(name="nama")
    private WebElement nama;

    @FindBy(id="simpan")
    private WebElement btnSimpan;

    public void simpan(){
        btnSimpan.click();
    }

    public void isiKode(String k) {
        kode.sendKeys(k);
    }

    public void isiNama(String n) {
        nama.sendKeys(n);
    }

    public FormEditSubjek(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }
}
