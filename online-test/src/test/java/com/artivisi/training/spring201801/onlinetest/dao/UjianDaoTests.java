package com.artivisi.training.spring201801.onlinetest.dao;

import com.artivisi.training.spring201801.onlinetest.entity.Ujian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(
    scripts = {"/sql/reset-data.sql", "/sql/sample-data-subjek.sql", "/sql/sample-data-ujian.sql"}
)
public class UjianDaoTests {

    @Autowired private UjianDao ujianDao;

    @Test
    public void testCariUjianBulanIni() {
        LocalDateTime bulanIni = LocalDateTime.of(2018, 11, 1, 0,0,0);
        List<Ujian> hasil = ujianDao.cariUjianBerdasarkanBulan(bulanIni, bulanIni.plusMonths(1));

        Assert.assertEquals("Jumlah ujian bulan ini tidak sesuai",
                 1, hasil.size());
    }
}
