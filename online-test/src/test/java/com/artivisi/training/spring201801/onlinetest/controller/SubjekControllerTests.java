package com.artivisi.training.spring201801.onlinetest.controller;

import com.artivisi.training.spring201801.onlinetest.pageobject.FormEditSubjek;
import com.artivisi.training.spring201801.onlinetest.pageobject.HalamanLogin;
import com.github.javafaker.Faker;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(
        scripts = {"classpath:/sql/reset-data.sql",
                "classpath:/sql/sample-data-subjek.sql"},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
)
public class SubjekControllerTests {

    private static final String URL_APLIKASI = "http://localhost";
    private static final String URL_LOGIN =  "/login";
    private static final String URL_FORM_SUBJEK = "/subjek/form";
    private static final String URL_DAFTAR_SUBJEK = "/subjek/list";

    @LocalServerPort
    private Integer port;

    private static HalamanLogin halamanLogin;
    private static FormEditSubjek formEditSubjek;

    private static WebDriver webDriver;
    private static WebDriverWait wait;

    @BeforeClass
    public static void setup() {
        webDriver = new FirefoxDriver();
        wait = new WebDriverWait(webDriver, 5);
        halamanLogin = new HalamanLogin(webDriver);
        formEditSubjek = new FormEditSubjek(webDriver);
    }

    @AfterClass
    public static void cleanup() {
        webDriver.quit();
    }

    @Before
    public void login(){
        String urlLogin = URL_APLIKASI + ":" + port + URL_LOGIN;
        System.out.println("URL halaman login : "+urlLogin);
        webDriver.get(urlLogin);
        halamanLogin.isiUsername("testmanager");
        halamanLogin.isiPassword("rahasia");
        halamanLogin.login();
        Boolean berhasilMasukDashboard = wait
                .until(ExpectedConditions.titleContains("Dashboard"));
        if (!berhasilMasukDashboard) {
            Assert.fail("Gagal masuk dashboard");
        }
    }

    @After
    public void logout() {
        webDriver.findElement(By.id("btnLogout")).click();
        if (!wait
                .until(ExpectedConditions.titleContains("Please sign in"))) {
            Assert.fail("Gagal Logout");
        }
    }

    @Test
    public void testDaftarSubjek() {
        String urlDaftarSubjek = URL_APLIKASI + ":" + port + URL_DAFTAR_SUBJEK;
        System.out.println("URL Daftar Subjek : "+urlDaftarSubjek);
        webDriver.get(urlDaftarSubjek);
        if (!wait.until(ExpectedConditions.titleContains("Daftar Subjek"))) {
            Assert.fail("Gagal buka daftar subjek");
        }
        Assert.assertTrue("Ada data subjek dengan kode J-001",
                webDriver.getPageSource().contains("J-001"));
    }

    @Test @Ignore
    public void testInsertSubjek() throws Exception {

        String urlFormSubjek = URL_APLIKASI + ":" + port + URL_FORM_SUBJEK;
        System.out.println("URL Form Subjek : "+urlFormSubjek);
        webDriver.get(urlFormSubjek);
        Boolean berhasilBukaFormSubjek = wait
                .until(ExpectedConditions.titleContains("Edit Subjek"));
        if (!berhasilBukaFormSubjek) {
            Assert.fail("Gagal buka form subjek");
        }

        Faker faker = new Faker();
        String kode = faker.number().digits(10);
        String nama = faker.book().genre();

        formEditSubjek.isiKode(kode);
        formEditSubjek.isiNama(nama);
        formEditSubjek.simpan();

        if (!wait.until(ExpectedConditions.titleContains("Daftar Subjek"))) {
            Assert.fail("Gagal buka daftar subjek");
        }

        Assert.assertTrue("Kode subjek ada",
                webDriver.getPageSource().contains(kode));

        Assert.assertTrue("Nama subjek ada",
                webDriver.getPageSource().contains(nama));

    }
}
