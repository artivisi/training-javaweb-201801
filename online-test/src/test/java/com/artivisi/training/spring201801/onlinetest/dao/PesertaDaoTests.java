package com.artivisi.training.spring201801.onlinetest.dao;

import com.artivisi.training.spring201801.onlinetest.entity.Peserta;
import com.github.javafaker.Faker;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.sql.ResultSet;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(
        scripts = {"/sql/reset-data.sql"}
)
public class PesertaDaoTests {

    private static final String SQL_HITUNG_PESERTA = "select count(*) from peserta";

    @Autowired private PesertaDao pesertaDao;
    @Autowired private DataSource dataSource;


    @Test
    public void testGeneratePeserta() throws Exception {
        Faker faker = new Faker();

        for (int i = 0; i < 50; i++) {
            String namadepan = faker.name().firstName();
            String namabelakang = faker.name().lastName();
            Peserta p = new Peserta();

            p.setNamaDepan(namadepan);
            p.setNamaBelakang(namabelakang);
            p.setEmail(namadepan.toLowerCase()+"."+namabelakang.toLowerCase()+"@"+faker.internet().domainName());
            p.setNomor(faker.number().digits(10));
            p.setNoHp(faker.phoneNumber().cellPhone());

            pesertaDao.save(p);
        }

        ResultSet hasilQuery
                = dataSource.getConnection()
                .prepareStatement(SQL_HITUNG_PESERTA)
                .executeQuery();

        Assert.assertTrue("Query ada hasilnya", hasilQuery.next());
        long jumlahPeserta = hasilQuery.getLong(1);
        Assert.assertEquals("Jumlah peserta ", 50, jumlahPeserta);

    }
}
