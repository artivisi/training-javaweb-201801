
CREATE TABLE daftar_peserta_ujian (
  id_ujian varchar(36) NOT NULL,
  id_peserta varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE paket_soal (
  id varchar(36) NOT NULL,
  kode varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE paket_soal_daftar_pertanyaan (
  paket_soal_id varchar(36) NOT NULL,
  daftar_pertanyaan_id varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE pertanyaan (
  id varchar(36) NOT NULL,
  kode varchar(100) NOT NULL,
  kunci_jawaban varchar(255) NOT NULL,
  pertanyaan varchar(255) NOT NULL,
  tingkat_kesulitan varchar(255) NOT NULL,
  tipe_pertanyaan varchar(255) NOT NULL,
  waktu_detik int(11) NOT NULL,
  subjek_id varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE peserta (
  id varchar(36) NOT NULL,
  email varchar(255) NOT NULL,
  nama varchar(255) DEFAULT NULL,
  no_hp varchar(255) DEFAULT NULL,
  nomor varchar(255) DEFAULT NULL,
  nama_belakang varchar(255) DEFAULT NULL,
  nama_depan varchar(255) DEFAULT NULL,
  foto varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE peserta_ujian (
  id varchar(36) NOT NULL,
  paket_soal_id varchar(36) DEFAULT NULL,
  peserta_id varchar(36) DEFAULT NULL,
  ujian_id varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE pilihan_jawaban (
  id varchar(36) NOT NULL,
  label varchar(255) DEFAULT NULL,
  urutan int(11) DEFAULT NULL,
  value varchar(255) DEFAULT NULL,
  pertanyaan_id varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE subjek (
  id varchar(36) NOT NULL,
  kode varchar(50) NOT NULL,
  nama varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE ujian (
  id varchar(36) NOT NULL,
  durasi_menit int(11) DEFAULT NULL,
  kode varchar(50) NOT NULL,
  waktu_pelaksanaan datetime(6) DEFAULT NULL,
  subjek_id varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


create table jawaban (
  id varchar(36) NOT NULL,
  id_pertanyaan varchar (36) not null,
  id_peserta varchar (36) not null,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE daftar_peserta_ujian
  ADD PRIMARY KEY (id_ujian,id_peserta),
  ADD KEY FK8rfyj6o6c9md5cr5u7idgdiqj (id_peserta);

ALTER TABLE paket_soal
  ADD PRIMARY KEY (id);

ALTER TABLE paket_soal_daftar_pertanyaan
  ADD PRIMARY KEY (paket_soal_id,daftar_pertanyaan_id),
  ADD KEY FK7ajhvr3qjoc18au6xisa8ouxv (daftar_pertanyaan_id);

ALTER TABLE pertanyaan
  ADD PRIMARY KEY (id),
  ADD KEY FK1qr3u434glbxyhfobi0tshur9 (subjek_id);

ALTER TABLE peserta
  ADD PRIMARY KEY (id);

ALTER TABLE peserta_ujian
  ADD PRIMARY KEY (id),
  ADD KEY FK6vbhjcl27yidolp7ycw5ktmj6 (paket_soal_id),
  ADD KEY FKai7ljqy75pjj6is374jcskdou (peserta_id),
  ADD KEY FKl850ak5liskughq1bd3lvnp0f (ujian_id);

ALTER TABLE pilihan_jawaban
  ADD PRIMARY KEY (id),
  ADD KEY FKm3wix0rjpwhpp266pfeu4c61t (pertanyaan_id);

ALTER TABLE subjek
  ADD PRIMARY KEY (id);

ALTER TABLE ujian
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY UK_9c64fqe90ccsw6uqfidiftp0g (kode),
  ADD KEY FKnadtgtf3ejvfocepli8431nab (subjek_id);


ALTER TABLE daftar_peserta_ujian
  ADD CONSTRAINT FK8rfyj6o6c9md5cr5u7idgdiqj FOREIGN KEY (id_peserta) REFERENCES peserta (id),
  ADD CONSTRAINT FKj8hwffcrff5hflsxvfhd9b3hk FOREIGN KEY (id_ujian) REFERENCES ujian (id);

ALTER TABLE paket_soal_daftar_pertanyaan
  ADD CONSTRAINT FK4h2newsjm93pqn3muunfnc22c FOREIGN KEY (paket_soal_id) REFERENCES paket_soal (id),
  ADD CONSTRAINT FK7ajhvr3qjoc18au6xisa8ouxv FOREIGN KEY (daftar_pertanyaan_id) REFERENCES pertanyaan (id);

ALTER TABLE pertanyaan
  ADD CONSTRAINT FK1qr3u434glbxyhfobi0tshur9 FOREIGN KEY (subjek_id) REFERENCES subjek (id);

ALTER TABLE peserta_ujian
  ADD CONSTRAINT FK6vbhjcl27yidolp7ycw5ktmj6 FOREIGN KEY (paket_soal_id) REFERENCES paket_soal (id),
  ADD CONSTRAINT FKai7ljqy75pjj6is374jcskdou FOREIGN KEY (peserta_id) REFERENCES peserta (id),
  ADD CONSTRAINT FKl850ak5liskughq1bd3lvnp0f FOREIGN KEY (ujian_id) REFERENCES ujian (id);

ALTER TABLE pilihan_jawaban
  ADD CONSTRAINT FKm3wix0rjpwhpp266pfeu4c61t FOREIGN KEY (pertanyaan_id) REFERENCES pertanyaan (id);

ALTER TABLE ujian
  ADD CONSTRAINT FKnadtgtf3ejvfocepli8431nab FOREIGN KEY (subjek_id) REFERENCES subjek (id);

ALTER TABLE jawaban
  ADD CONSTRAINT fkjawabanpeserta FOREIGN KEY (id_peserta) REFERENCES peserta (id);

ALTER TABLE jawaban
  ADD CONSTRAINT fkjawabanpertanyaan FOREIGN KEY (id_pertanyaan) REFERENCES pertanyaan (id);
