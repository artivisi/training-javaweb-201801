package com.artivisi.training.spring201801.onlinetest.dao;

import com.artivisi.training.spring201801.onlinetest.entity.Subjek;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface SubjekDao extends PagingAndSortingRepository<Subjek, String> {

    @Query("select s from Subjek s order by s.kode")
    Set<Subjek> semuaSubjek();
}
