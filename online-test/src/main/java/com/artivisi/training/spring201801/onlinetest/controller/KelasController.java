package com.artivisi.training.spring201801.onlinetest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class KelasController {
    @GetMapping("/kelas/list")
    public void daftarKelas(){

    }

    @GetMapping("/kelas/form")
    public void editKelas(){

    }

    @PostMapping("/kelas/form")
    public String simpanKelas(){
        return "redirect:list";
    }
}
