package com.artivisi.training.spring201801.onlinetest.entity;

public enum TingkatKesulitan {
    MUDAH("Mudah"),
    SEDANG("Sedang"),
    SULIT("Sulit");

    private String displayLabel;

    TingkatKesulitan(String label){
        this.displayLabel = label;
    }

    public String getDisplayLabel() {
        return this.displayLabel;
    }
}
