package com.artivisi.training.spring201801.onlinetest.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity @Data
public class Pertanyaan {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull @NotBlank @Size(min = 3, max = 100)
    private String kode;

    @NotNull @NotBlank
    private String pertanyaan;

    @NotNull @Min(10)
    private Integer waktuDetik = 60;

    @ManyToOne @NotNull
    private Subjek subjek;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TipePertanyaan tipePertanyaan = TipePertanyaan.PILIHAN_GANDA;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TingkatKesulitan tingkatKesulitan = TingkatKesulitan.SEDANG;

    @NotNull @NotBlank
    private String kunciJawaban;

}
