package com.artivisi.training.spring201801.onlinetest.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity @Data
public class Ujian {
    @Id @Column(length = 36)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(length = 50, nullable = false, unique = true)
    private String kode;

    @ManyToOne
    private Subjek subjek;
    private LocalDateTime waktuPelaksanaan;

    private Integer durasiMenit;

    @ManyToMany
    @JoinTable(
            name = "daftar_peserta_ujian",
            joinColumns = @JoinColumn(name = "id_ujian"),
            inverseJoinColumns = @JoinColumn(name = "id_peserta")
    )
    private Set<Peserta> daftarPeserta = new HashSet<>();
}

