package com.artivisi.training.spring201801.onlinetest.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity @Data
public class Jawaban {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_pertanyaan")
    private Pertanyaan pertanyaan;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_peserta")
    private Peserta peserta;
}
