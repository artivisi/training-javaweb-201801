package com.artivisi.training.spring201801.onlinetest.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity @Data
public class PesertaUjian {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    private Peserta peserta;

    @ManyToOne
    private Ujian ujian;

    @ManyToOne
    private PaketSoal paketSoal;
}
