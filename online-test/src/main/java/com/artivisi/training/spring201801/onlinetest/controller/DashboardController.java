package com.artivisi.training.spring201801.onlinetest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

@Controller
public class DashboardController {

    @GetMapping({"/home", "/", "/coba/**"})
    public String tampilkanDashboard(HttpSession session){
        session.setAttribute("username", "endy");
        return "dashboard";
    }
}
