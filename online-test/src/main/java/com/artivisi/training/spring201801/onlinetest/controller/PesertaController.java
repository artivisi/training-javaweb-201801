package com.artivisi.training.spring201801.onlinetest.controller;

import com.artivisi.training.spring201801.onlinetest.dao.PesertaDao;
import com.artivisi.training.spring201801.onlinetest.entity.Peserta;
import com.artivisi.training.spring201801.onlinetest.entity.Subjek;
import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

@Controller @RequestMapping("/peserta")
public class PesertaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PesertaController.class);

    @Value("classpath:jrxml/daftar-peserta.jrxml") private Resource templateDaftarPeserta;
    @Value("classpath:template-sertifikat.odt") private Resource templateSertifikat;
    //@Value("classpath:sertifikat-peserta.odp") private Resource templateSertifikat;
    @Value("${lokasi.foto}") private String lokasiFoto;
    @Autowired private PesertaDao pesertaDao;

    private JasperReport reportDaftarPeserta;

    @GetMapping("/{id}/foto")
    public void fotoPeserta(@PathVariable(value = "id") Peserta peserta, HttpServletResponse response) {
        response.setContentType("image/jpeg");
        File foto = new File(lokasiFoto
                + File.separator + peserta.getFoto());
        try {
            FileCopyUtils.copy(new FileInputStream(foto), response.getOutputStream());
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @GetMapping("/list")
    public ModelMap dataPeserta(@PageableDefault(sort = {"nomor"}, size = 9) Pageable page){
        return new ModelMap("dataPeserta", pesertaDao.findAll(page));
    }


    @GetMapping("/form")
    public ModelMap tampilkanForm() {
        ModelMap mm = new ModelMap();
        mm.addAttribute("peserta", new Peserta());
        return mm;
    }

    @PostMapping("/form")
    public String prosesForm(@ModelAttribute @Valid Peserta peserta,
                             BindingResult err,
                             SessionStatus status,
                             @RequestParam(required = false) MultipartFile fileFoto,
                             RedirectAttributes flashData){

        if (err.hasErrors()) {
            return "/peserta/form";
        }

        LOGGER.debug("Nama File : {}",fileFoto.getName());
        LOGGER.debug("Nama File Asli : {}",fileFoto.getOriginalFilename());
        LOGGER.debug("Ukuran : {}",fileFoto.getSize());
        LOGGER.debug("Jenis File : {}",fileFoto.getContentType());

        simpanFileFoto(peserta, fileFoto);
        pesertaDao.save(peserta);
        LOGGER.debug("Peserta : {}",peserta);

        flashData.addFlashAttribute("hasil", "Data peserta berhasil disimpan");

        return "redirect:/peserta/form";
    }

    @GetMapping("/{peserta}/{subjek}/sertifikat")
    public void generateSertifikat(@PathVariable Peserta peserta,
                                   @PathVariable Subjek subjek,
                                   HttpServletResponse response) {

        try {
            // 1. Load template dari file
            InputStream in = templateSertifikat.getInputStream();

            // 2. Inisialisasi template engine, menentukan sintaks penulisan variabel
            IXDocReport report = XDocReportRegistry.getRegistry().
                    loadReport(in, TemplateEngineKind.Freemarker);

            // 3. Context object, untuk mengisi variabel
            IContext ctx = report.createContext();
            ctx.put("nama", peserta.getNamaLengkap());
            ctx.put("subjek", subjek.getKode()+" - "+subjek.getNama());
            ctx.put("instruktur", "Mr. Instruktur");
            ctx.put("tanggal", LocalDate.now()
                    .format(DateTimeFormatter.ofPattern("EEE, dd MMM yyyy", new Locale("id", "id"))));

            // 4. Konfigurasi format file
            response.setContentType("application/pdf");

            response.setHeader("Content-Disposition",
                    "attachment; filename=sertifikat-"
                            + subjek.getKode()
                            + "-"
                            + peserta.getNomor()
                            + ".pdf");

            Options options = Options.getTo(ConverterTypeTo.PDF);

            // 4. Kirim ke browser
            OutputStream out = response.getOutputStream();
            report.convert(ctx, options, out);
            out.flush();

        } catch (Exception err) {
            LOGGER.error(err.getMessage(), err);
        }
    }

    @GetMapping("/daftar")
    public void daftarPesertaPdf(HttpServletResponse response) {
        try {

            // 1. Datasource untuk ditampilkan di band detail
            JRBeanCollectionDataSource dataPeserta
                    = new JRBeanCollectionDataSource(pesertaDao.semuaPeserta());

            // 2. Parameter report
            LocalDateTime sekarang = LocalDateTime.now();
            Map<String, Object> params = new HashMap<>();
            params.put("judulDokumen", "Daftar Peserta Pelatihan");
            params.put("terakhirUpdate", sekarang);

            // 3. Merge template dengan data
            JasperPrint jrPrint = JasperFillManager
                    .fillReport(getReport(), params, dataPeserta);

            // 4. Render PDF
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition",
                    "attachment; filename=daftar-peserta-"
                            + sekarang.format(DateTimeFormatter.ofPattern("yyyyMMddHHmm"))
                            + ".pdf");
            JasperExportManager.exportReportToPdfStream(jrPrint, response.getOutputStream());
        } catch (Exception err) {
            LOGGER.error(err.getMessage(), err);
        }
    }

    private JasperReport getReport(){
        if (reportDaftarPeserta == null) {
            try {
                reportDaftarPeserta = JasperCompileManager.compileReport(templateDaftarPeserta.getInputStream());
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        return reportDaftarPeserta;
    }

    private void simpanFileFoto(Peserta peserta, MultipartFile fileFoto) {
        File lokasiPenyimpanan = new File(lokasiFoto);
        if(!lokasiPenyimpanan.exists()){
            lokasiPenyimpanan.mkdirs();
        }

        LOGGER.debug("Lokasi penyimpanan : {}", lokasiPenyimpanan.getAbsolutePath());
        String namaAsliFile = fileFoto.getOriginalFilename();
        String extension = namaAsliFile.substring(namaAsliFile.lastIndexOf("."));
        String namaBaru = UUID.randomUUID().toString() + extension;
        LOGGER.debug("Nama file yang disimpan : "+namaBaru);

        peserta.setFoto(namaBaru);

        try {
            fileFoto.transferTo(new File(lokasiPenyimpanan.getAbsolutePath()
                    + File.separator
                    + namaBaru));
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
