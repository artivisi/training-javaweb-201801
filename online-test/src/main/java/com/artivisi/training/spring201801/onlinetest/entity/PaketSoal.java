package com.artivisi.training.spring201801.onlinetest.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Entity @Data
public class PaketSoal {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String kode;

    @ManyToMany
    private Set<Pertanyaan> daftarPertanyaan = new HashSet<>();
}
