package com.artivisi.training.spring201801.onlinetest.controller;

import com.artivisi.training.spring201801.onlinetest.dao.SubjekDao;
import com.artivisi.training.spring201801.onlinetest.entity.Subjek;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class SubjekController {

    @Autowired private SubjekDao subjekDao;

    @GetMapping("/subjek/list")
    public ModelMap daftarSubjek(@PageableDefault(sort = {"kode"}) Pageable page){
        Page<Subjek> hasilQuery = subjekDao.findAll(page);
        return new ModelMap()
                .addAttribute("dataSubjek", hasilQuery);
    }

    @PreAuthorize("hasAuthority('EDIT_SUBJEK')")
    @GetMapping("/subjek/form")
    public ModelMap tampilkanForm(@RequestParam(value = "id", required = false) Subjek sub){

        if(sub == null) {
            sub = new Subjek();
        }

        return new ModelMap()
                .addAttribute("subjek", sub);
    }

    @PreAuthorize("hasAuthority('EDIT_SUBJEK')")
    @PostMapping("/subjek/form")
    public String prosesForm(@ModelAttribute @Valid Subjek s, BindingResult daftarError, SessionStatus status){
        System.out.println("Kode : "+s.getKode());
        System.out.println("Nama : "+s.getNama());
        if(daftarError.hasErrors()) {
            return "/subjek/form";
        } else {
            subjekDao.save(s);
            status.setComplete();
            return "redirect:list";
        }

    }
}
