package com.artivisi.training.spring201801.onlinetest.controller;

import com.artivisi.training.spring201801.onlinetest.dao.PertanyaanDao;
import com.artivisi.training.spring201801.onlinetest.dao.SubjekDao;
import com.artivisi.training.spring201801.onlinetest.entity.Pertanyaan;
import com.artivisi.training.spring201801.onlinetest.entity.Subjek;
import com.artivisi.training.spring201801.onlinetest.entity.TingkatKesulitan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.util.Set;

@Controller
public class PertanyaanController {
    @Autowired private SubjekDao subjekDao;
    @Autowired private PertanyaanDao pertanyaanDao;

    @ModelAttribute("daftarSubjek")
    public Set<Subjek> daftarSubjek() {
        return subjekDao.semuaSubjek();
    }

    @ModelAttribute("pilihanTingkatKesulitan")
    public TingkatKesulitan[] pilihanTingkatKesulitan(){
        return TingkatKesulitan.values();
    }

    @GetMapping("/pertanyaan/form")
    public ModelMap tampilkanForm(@RequestParam(value = "id", required = false) Pertanyaan p) {
        ModelMap mm = new ModelMap();

        if(p == null) {
            mm.addAttribute("pesan", "Harap hubungi administrator");
            p = new Pertanyaan();
        }

        return mm.addAttribute("pertanyaan", p);
    }

    @PostMapping("/pertanyaan/form")
    public String prosesForm(@ModelAttribute @Valid Pertanyaan p, BindingResult err, SessionStatus status) {
        if (err.hasErrors()) {
            return "/pertanyaan/form";
        }

        System.out.println("Pertanyaan : "+p);

        pertanyaanDao.save(p);
        status.setComplete();

        return "redirect:list";
    }
}
