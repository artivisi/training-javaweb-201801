package com.artivisi.training.spring201801.onlinetest.dao;

import com.artivisi.training.spring201801.onlinetest.entity.Ujian;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface UjianDao extends PagingAndSortingRepository<Ujian, String> {

    @Query("select u from Ujian u where u.waktuPelaksanaan >= :mulai " +
            "and u.waktuPelaksanaan < :sampai")
    List<Ujian> cariUjianBerdasarkanBulan(@Param("mulai") LocalDateTime mulai,
                                          @Param("sampai") LocalDateTime sampai);
}
