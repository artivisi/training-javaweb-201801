package com.artivisi.training.spring201801.onlinetest.dao;

import com.artivisi.training.spring201801.onlinetest.entity.Peserta;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PesertaDao extends PagingAndSortingRepository<Peserta, String> {

    @Query("select p from Peserta p order by p.nomor")
    List<Peserta> semuaPeserta();
}
