package com.artivisi.training.spring201801.onlinetest.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity @Data
public class Peserta {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String nomor;
    private String namaDepan;
    private String namaBelakang;

    @NotNull @NotBlank @Email
    private String email;

    private String noHp;
    private String foto;

    public String getNamaLengkap(){
        return namaDepan + " " + namaBelakang;
    }
}
