package com.artivisi.training.spring201801.onlinetest.dao;

import com.artivisi.training.spring201801.onlinetest.entity.Pertanyaan;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PertanyaanDao extends PagingAndSortingRepository<Pertanyaan, String> {
}
