package com.artivisi.training.spring201801.onlinetest.entity;

public enum TipePertanyaan {
    PILIHAN_GANDA("Pilihan Ganda"),
    ESAI("Esai");

    private String displayLabel;

    TipePertanyaan(String label){
        this.displayLabel = label;
    }

    public String getDisplayLabel() {
        return this.displayLabel;
    }
}
