## Membuat Project Maven #

Referensi : [https://software.endy.muhardin.com/java/apache-maven/](https://software.endy.muhardin.com/java/apache-maven/)

## Perintah-perintah Maven ##

* Compile dan buat `jar`

        mvn package

* Menghapus hasil compile

        mvn clean

* Bersihkan, kemudian compile dan buat jar

        mvn clean package

* Menjalankan class Java yang ada method `main`nya

        mvn exec:java -Dexec.mainClass=Halo
