create table karyawan (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nip VARCHAR(100) not null,
    nama VARCHAR(255) not null,
    unique(nip)
);