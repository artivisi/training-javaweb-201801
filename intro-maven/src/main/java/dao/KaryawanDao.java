package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Karyawan;

public class KaryawanDao {

    private static final String SQL_HITUNG_KARYAWAN = "select count(*) from karyawan";
    private static final String SQL_SEMUA_KARYAWAN = "select * from karyawan";

    private Connection koneksiDatabase;

    public KaryawanDao(Connection conn) {
        koneksiDatabase = conn;
    }

    public void simpan(Karyawan k) {
        System.out.println("Menyimpan data karyawan ke database");
    }

    public Long jumlahKaryawan(){

        try {
            ResultSet rs = koneksiDatabase
            .prepareStatement(SQL_HITUNG_KARYAWAN)
            .executeQuery();

            if(rs.next()) {
                return rs.getLong(1);
            } 
        } catch (Exception err) {
            err.printStackTrace();
        }

        return 0L;

    }

    public List<Karyawan> semuaKaryawan() {
        List<Karyawan> hasil = new ArrayList<>();

        try {
            ResultSet rs = koneksiDatabase
            .prepareStatement(SQL_SEMUA_KARYAWAN)
            .executeQuery();

            while(rs.next()){
                Karyawan k = new Karyawan();
                k.setId(rs.getInt("id"));
                k.setNip(rs.getString("nip"));
                k.setNama(rs.getString("nama"));
                hasil.add(k);
            }

        } catch (Exception err) {
            err.printStackTrace();
        }

        return hasil;
    }
}