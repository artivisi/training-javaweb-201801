import model.Karyawan;
import ui.KaryawanForm;

import java.util.List;

import java.sql.Connection;
import java.sql.DriverManager;

import dao.KaryawanDao;

public class Halo {

  private static final String JDBC_URL = "jdbc:mysql://localhost/belajar?serverTimezone=UTC";
  private static final String JDBC_USERNAME = "root";
  private static final String JDBC_PASSWORD = "";

  public static void main(String[] abc) throws Exception {
    System.out.println("Halo Java");

    Connection conn = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);

    KaryawanDao kd = new KaryawanDao(conn);
    Long jml = kd.jumlahKaryawan();
    System.out.println("Jumlah : "+jml);

    List<Karyawan> data = kd.semuaKaryawan();
    for(Karyawan k : data){
      System.out.println(k);
    }
  }
}