package model;

import lombok.Data;

@Data
public class Karyawan {
    private Integer id;
    private String nip;
    private String nama;
}