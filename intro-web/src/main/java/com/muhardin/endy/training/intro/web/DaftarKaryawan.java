package com.muhardin.endy.training.intro.web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.KaryawanDao;
import model.Karyawan;

@WebServlet("/karyawan/list")
public class DaftarKaryawan extends HttpServlet {

    private static final String JDBC_URL = "jdbc:mysql://localhost/belajar?serverTimezone=UTC";
    private static final String JDBC_USERNAME = "root";
    private static final String JDBC_PASSWORD = "";

    private Connection koneksiDatabase;
    private KaryawanDao karyawanDao;

    public void init() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            koneksiDatabase = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);
            karyawanDao = new KaryawanDao(koneksiDatabase);
        } catch (Exception e) {
            e.printStackTrace();
		}
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response){
        try {
            List<Karyawan> data = karyawanDao.semuaKaryawan();
            String output = "<html><head><title>Daftar Karyawan</title></head><body>\n";
            output += "<h1>Data Karyawan</h1>\n";
            output += "<table><tr><td>NIP</td><td>Nama</td></tr>\n";

            for(Karyawan k : data) {
                output += "<tr>\n";
                output += "<td>"+k.getNip()+"</td>";
                output += "<td>"+k.getNama()+"</td>";
                output += "</tr>\n";
            }

            output += "</table>";
            output += "</html>";

            response.getWriter().print(output);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
		}
    }
}