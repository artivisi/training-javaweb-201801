package com.artivisi.training.spring201801.denganioc;

import com.artivisi.training.spring201801.Karyawan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class KaryawanDao {
    private static final String SQL_INSERT = "insert into karyawan (nip, nama) values (?,?)";

    private Connection koneksiDatabase;

    public KaryawanDao(Connection koneksi){
        this.koneksiDatabase = koneksi;
    }

    /*
    public void setKoneksiDatabase(Connection koneksiDatabase) {
        this.koneksiDatabase = koneksiDatabase;
    }
    */

    public void simpan(Karyawan k){
        try {
            PreparedStatement ps = koneksiDatabase.prepareStatement(SQL_INSERT);
            ps.setString(1, k.getNip());
            ps.setString(2, k.getNama());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Iterable<Karyawan> semuaKaryawan(){
        return new ArrayList<>();
    }
}
