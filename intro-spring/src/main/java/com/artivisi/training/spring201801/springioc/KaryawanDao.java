package com.artivisi.training.spring201801.springioc;

import com.artivisi.training.spring201801.Karyawan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Component
public class KaryawanDao {
    private static final String SQL_INSERT = "insert into karyawan (nip, nama) values (?,?)";

    @Autowired
    private DataSource dataSource;

    /* // diganti dengan annotation @Autowired
    public KaryawanDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    */

    public void simpan(Karyawan k){
        try {
            PreparedStatement ps = dataSource.getConnection()
                    .prepareStatement(SQL_INSERT);
            ps.setString(1, k.getNip());
            ps.setString(2, k.getNama());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
