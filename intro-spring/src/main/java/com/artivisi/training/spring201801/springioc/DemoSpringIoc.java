package com.artivisi.training.spring201801.springioc;

import com.artivisi.training.spring201801.Departemen;
import com.artivisi.training.spring201801.Karyawan;
import com.github.javafaker.Faker;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DemoSpringIoc {

    private static final String JDBC_URL = "jdbc:mysql://localhost/belajar?serverTimezone=UTC";
    private static final String JDBC_USERNAME = "root";
    private static final String JDBC_PASSWORD = "";

    public static void main(String[] args) {

        // menginstankan secara manual
        /*
        MysqlDataSource ds = new MysqlDataSource();
        ds.setURL(JDBC_URL);
        ds.setUser(JDBC_USERNAME);
        ds.setPassword(JDBC_PASSWORD);

        KaryawanDao kd = new KaryawanDao(ds);
        */

        Faker faker = new Faker();

        //ApplicationContext spring = new ClassPathXmlApplicationContext("classpath:ioc.xml");
        ApplicationContext spring = new AnnotationConfigApplicationContext(Konfigurasi.class);

        KaryawanDao kd = spring.getBean(KaryawanDao.class);

        String nip = faker.number().digits(5);
        String nama = faker.name().fullName();
        Karyawan k = new Karyawan();
        k.setNip(nip);
        k.setNama(nama);

        kd.simpan(k);
        System.out.println("Menyimpan karyawan : "+k);

        DepartemenDao dd = spring.getBean(DepartemenDao.class);

        Departemen d = new Departemen();
        d.setKode(faker.number().digits(3));
        d.setNama(faker.company().industry());

        dd.simpan(d);
        System.out.println("Menyimpan departemen : "+d);
    }
}
