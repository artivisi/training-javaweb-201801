package com.artivisi.training.spring201801.denganioc;

import com.artivisi.training.spring201801.Departemen;
import com.artivisi.training.spring201801.Karyawan;

import java.sql.Connection;
import java.sql.DriverManager;

public class DemoIoc {
    private static final String JDBC_URL = "jdbc:mysql://localhost/belajar?serverTimezone=UTC";
    private static final String JDBC_USERNAME = "root";
    private static final String JDBC_PASSWORD = "";

    public static void main(String[] args) {
        Karyawan k = new Karyawan();
        k.setNip("997");
        k.setNama("Karyawan 997");

        Departemen d = new Departemen();
        d.setKode("TIK");
        d.setNama("Teknologi Informasi dan Komputer");

        // buat koneksi database
        try {
            Connection koneksiDatabase = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);

            KaryawanDao kd = new KaryawanDao(koneksiDatabase);
            kd.simpan(k);

            DepartemenDao dd = new DepartemenDao(koneksiDatabase);
            dd.simpan(d);

            koneksiDatabase.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
