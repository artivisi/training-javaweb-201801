package com.artivisi.training.spring201801.tanpaioc;

import com.artivisi.training.spring201801.Karyawan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class KaryawanDao {
    private static final String JDBC_URL = "jdbc:mysql://localhost/belajar?serverTimezone=UTC";
    private static final String JDBC_USERNAME = "root";
    private static final String JDBC_PASSWORD = "";

    private static final String SQL_INSERT = "insert into karyawan (nip, nama) values (?,?)";

    private Connection koneksiDatabase;

    public KaryawanDao(){
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            koneksiDatabase = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void simpan(Karyawan k){
        try {
            PreparedStatement ps = koneksiDatabase.prepareStatement(SQL_INSERT);
            ps.setString(1, k.getNip());
            ps.setString(2, k.getNama());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Iterable<Karyawan> semuaKaryawan(){
        return new ArrayList<>();
    }
}
