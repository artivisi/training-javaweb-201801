package com.artivisi.training.spring201801.denganioc;

import com.artivisi.training.spring201801.Departemen;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class DepartemenDao {

    private static final String SQL_INSERT = "insert into departemen (kode, nama) values (?,?)";

    private Connection koneksiDatabase;

    public DepartemenDao(Connection koneksiDatabase) {
        this.koneksiDatabase = koneksiDatabase;
    }

    public void simpan(Departemen d){
        try {
            PreparedStatement ps = koneksiDatabase.prepareStatement(SQL_INSERT);
            ps.setString(1, d.getKode());
            ps.setString(2, d.getNama());
            ps.executeUpdate();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }
}
