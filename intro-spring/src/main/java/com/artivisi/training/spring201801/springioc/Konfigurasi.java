package com.artivisi.training.spring201801.springioc;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@ComponentScan("com.artivisi.training.spring201801.springioc")
public class Konfigurasi {

    private static final String JDBC_URL = "jdbc:mysql://localhost/belajar?serverTimezone=UTC";
    private static final String JDBC_USERNAME = "root";
    private static final String JDBC_PASSWORD = "";

    /* sama dengan deklarasi berikut di XML
    <bean id="dataSource" class="com.mysql.cj.jdbc.MysqlDataSource">
        <property name="URL" value="jdbc:mysql://localhost/belajar?serverTimezone=UTC" />
        <property name="user" value="root" />
        <property name="password" value="" />
    </bean>
    */
    @Bean
    public DataSource buatDataSource(){
        MysqlDataSource ds = new MysqlDataSource();
        ds.setURL(JDBC_URL);
        ds.setUser(JDBC_USERNAME);
        ds.setPassword(JDBC_PASSWORD);
        return ds;
    }
}
