package com.artivisi.training.spring201801.springioc;

import com.artivisi.training.spring201801.Departemen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.PreparedStatement;

@Component
public class DepartemenDao {
    private static final String SQL_INSERT = "insert into departemen (kode, nama) values (?,?)";

    @Autowired private DataSource dataSource;

    public void simpan(Departemen d){
        try {
            PreparedStatement ps = dataSource.getConnection()
                    .prepareStatement(SQL_INSERT);
            ps.setString(1, d.getKode());
            ps.setString(2, d.getNama());
            ps.executeUpdate();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }
}
