package com.artivisi.training.spring201801;

import lombok.Data;

@Data
public class Departemen {
    private Integer id;
    private String kode;
    private String nama;
}
