package com.artivisi.training.spring201801.tanpaioc;

import com.artivisi.training.spring201801.Karyawan;

public class DemoTanpaIoc {
    public static void main(String[] args) {
        Karyawan k = new Karyawan();
        k.setNip("999");
        k.setNama("Karyawan 999");

        KaryawanDao kd = new KaryawanDao();
        kd.simpan(k);
    }
}
