package com.artivisi.training.spring201801;

import lombok.Data;

@Data
public class Karyawan {
    private Integer id;
    private String nip;
    private String nama;
}
