create table departemen (
  id INT PRIMARY KEY AUTO_INCREMENT,
  kode varchar (10) not null,
  nama varchar (255) not null,
  unique (kode)
);