# Intro Java #

* Source code java berbentuk file teks, bisa diedit dengan editor apa saja. Dianjurkan yang ada warnanya (syntax highlighting) seperti Notapad++, Visual Studio Code, dan sebagainya
* Nama file diakhiri dengan ekstension `.java`, misalnya `Halo.java`
* Satu file boleh berisi beberapa class, tapi cuma boleh satu `public` class.
* Nama file harus sama dengan nama `public` class. Kalau dalam file tidak ada `public` class, nama file bebas saja.
* File source code dikompilasi dengan perintah `javac`:

        javac Halo.java

    dan akan menghasilkan file `.class`, misalnya `Halo.class`
* Class yang bisa dijalankan hanya yang memiliki method `public static void main` dengan argumen `String[]`. Contohnya seperti ini:

    ```java
    public static void main(String[] x) {
        System.out.println("Halo");
    }
    ```
* Untuk menjalankan method main, gunakan perintah `java` di command line dengan menyebutkan *nama class* (bukan *nama file*)

        java Halo

* Bila menggunakan package, file source code akan tersebar dalam folder. Kompilasinya dilakukan dengan opsi `-sourcepath` untuk menunjukkan folder tempat source code berada

        javac -sourcepath src -d bin src/*.java

* Pada saat menjalankan aplikasi, perintah `java` akan mencari class di folder tempat dia dijalankan. Bila file class ada di lokasi berbeda, kita harus beritahu dengan opsi `-cp`, artinya `CLASSPATH` atau path tempat class berada

        java -cp bin Halo

* Untuk mempaketkan class dan package yang sudah kita buat, di Java ada format file `jar`. Cara membuatnya sama dengan membuat file `zip` biasa. Bisa menggunakan aplikasi WinZip atau WinRAR. Pastikan algoritma yang dipakai adalah ZIP.

* Membuat jar bisa juga dengan perintah command line :

        jar cvf aplikasi-saya.jar -C bin .

    atau bisa juga dengan cara masuk ke folder `bin` kemudian membuat jar file di posisi itu

        cd bin
        jar cvf ../aplikasi-saya.jar .

* Isi jar bisa dicek dengan opsi `tvf`

        jar tvf aplikasi-saya.jar
    
    pastikan isinya langsung nama package, tidak didahului folder `bin`

        0    Wed Oct 31 11:10:56 WIB 2018 META-INF/
        69   Wed Oct 31 11:10:56 WIB 2018 META-INF/MANIFEST.MF
        6148 Wed Oct 31 11:02:14 WIB 2018 .DS_Store
        0    Wed Oct 31 10:22:02 WIB 2018 dao/
        613  Wed Oct 31 10:26:10 WIB 2018 dao/KaryawanDao.class
        756  Wed Oct 31 10:26:10 WIB 2018 Halo.class
        0    Wed Oct 31 10:22:02 WIB 2018 model/
        246  Wed Oct 31 10:26:10 WIB 2018 model/Karyawan.class
        0    Wed Oct 31 10:26:10 WIB 2018 ui/
        313  Wed Oct 31 10:26:10 WIB 2018 ui/KaryawanForm.class
        315  Wed Oct 31 10:24:56 WIB 2018 ui/KaryawanInput.class

* Aplikasi Java terdiri dari banyak package, berisi banyak class, berisi banyak method dan properties/variable. Ilustrasinya bisa dilihat di gambar berikut

[![Anatomi Aplikasi Java]('docs/anatomi-aplikasi-java.jpg')]('docs/anatomi-aplikasi-java.jpg')